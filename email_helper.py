from email.mime.multipart import MIMEMultipart
import dominate
import smtplib, ssl

from dominate.tags import *
from email.mime.application import MIMEApplication
from email.mime.text import MIMEText
from os.path import basename
#from email.mime.multipart import MIMEMultipart

smtp_server = "smtp.mail.yahoo.com"
port = 587
sender_email = "suncowiam@yahoo.com"
password = "ndbzvqevugbwzszj"  # yahoo
context = ssl.create_default_context()

def create_simple_html(message):
    assert message != None, print("No message to send")
    lines = message.split('\n')

    doc:dominate.document = dominate.document(title='Property Report')
    with doc.head:
        '''
        link(rel='stylesheet', href='style.css')
        script(type='text/javascript', src='script.js')
        style("""\
            body {
                whitespace: pre;
                background-color: #F9F8F1;
                color: #2C232A;
                font-family: monospace;
                font-size: 2.6em;
                margin: 3em 1em;
            }

        """)
        '''

    with doc:
        for line in lines:
            if "http" in line:
                doc.add(a(line, href=line))
                doc.add(font(pre('\r'), face='monospace'))
                pass
            else:
                doc.add(font(pre(line+'\r'), face='monospace'))
    
    return str(doc)


def create_email_mime(sender_email, receiver_emails, subject:str, plain:str, html=None, files:list=None) -> str:
    if html is None:
        html = create_simple_html(plain)
    
    message = MIMEMultipart('alternative')
    message['Subject'] = subject
    message['From'] = sender_email
    message['To'] = ", ".join(receiver_emails)

    message.attach(MIMEText(plain, "plain"))
    message.attach(MIMEText(html, "html"))

    if files:
        for f in files:
            with open(f, "rb") as fil:
                part = MIMEApplication(
                    fil.read(),
                    Name=basename(f)
                )
            # After the file is closed
            part['Content-Disposition'] = 'attachment; filename="%s"' % basename(f)
            message.attach(part)

    return message.as_string()


def send_email(receiver_emails:list, subject, message, files:list=None):
    try:
        server = smtplib.SMTP(smtp_server,port)
        server.ehlo() # Can be omitted
        server.starttls(context=context) # Secure the connection
        server.ehlo() # Can be omitted
        server.login(sender_email, password)
        # TODO: Send email here

        #message['To'] = receiver_emails
        str_message = create_email_mime(sender_email, receiver_emails, subject, plain=message, html=None, files=files)
        server.sendmail(sender_email, receiver_emails, msg=str_message)
    except Exception as e:
        # Print any error messages to stdout
        print(e)
        return False
    finally:
        server.quit()
    return True

if __name__ == "__main__":
    print("Testing Email")

    send_email(["tuan.sy.tran@gmail.com", "suncowiam@yahoo.com"], subject="Test", message="hello i'm here")
